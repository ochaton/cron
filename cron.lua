local M = {}

local seconds = {0, 59}
local minutes = {0, 59}
local hours   = {0, 23}
local dom     = {1, 31}
local months  = {1, 12, {
	jan = 1,  feb = 2,  mar = 3,
	apr = 4,  may = 5,  jun = 6,
	jul = 7,  aug = 8,  sep = 9,
	oct = 10, nov = 11, dec = 12,
}}

local dow = {0, 6, {
	sun = 0, mon = 1, tue = 2,
	wed = 3, thu = 4, fri = 5, sat = 6,
}}

local function get_bits(min, max, step)
	local bits = 0ULL
	step = step or 1
	for i = min, max, step do
		bits = bits + 2ULL^i
	end
	return bits
end

local starBit = 2ULL^63

local function all(bounds)
	return bit.bor(get_bits(bounds[1], bounds[2], 1), starBit)
end

local function parse_descriptor(spec)
	if spec == '@yearly' or spec == '@annually' then
		return {
			second = 1, minute = 1, hour = 1, dom = 2^1, month = 2^1, dow = all(dow),
		}
	elseif spec == '@monthly' then
		return {
			second = 1, minute = 1, hour = 1, dom = 2^1, month = all(months), dow = all(dow),
		}
	elseif spec == '@weekly' then
		return {
			second = 1, minute = 1, hour = 1, dom = all(dom), month = all(months), dow = 1,
		}
	elseif spec == '@daily' or spec == '@midnight' then
		return {
			second = 1, minute = 1, hour = 1, dom = all(dom), month = all(months), dow = all(dow),
		}
	elseif spec == '@hourly' then
		return {
			second = 1, minute = 1, hour = all(hours), dom = all(dom), month = all(months), dow = all(dow),
		}
	else
		return false, 'unknown descriptor'
	end
end

local function get_range(expr, bounds)
	local ras = expr:split("/")
	local loh = ras[1]:split("-")
	local single = #loh == 1

	local start, stop, step
	local extra = 0ULL

	if loh[1] == "*" or loh[1] == "?" then
		start = bounds[1]
		stop = bounds[2]
		extra = starBit
	else
		start = tonumber(loh[1])
		if not start then
			if bounds[3] and bounds[3][loh[1]:lower()] then
				start = bounds[3][loh[1]:lower()]
			else
				error(("malformed start %s given for range %s"):format(loh[1], expr), 2)
			end
		end

		if #loh == 1 then
			stop = start
		elseif #loh == 2 then
			stop = tonumber(loh[2])
			if not stop then
				if bounds[3] and bounds[3][loh[2]:lower()] then
					stop = bounds[3][loh[2]:lower()]
				else
					error(("malformed stop %s given for range %s"):format(loh[2], expr), 2)
				end
			end
		else
			error(("too many hyphens in range %s"):format(expr), 2)
		end

	end

	if #ras == 1 then
		step = 1
	elseif #ras == 2 then
		step = assert(tonumber(ras[2]), expr)

		-- N/step
		if single then
			stop = bounds[2]
		end
		if step > 1 then
			extra = 0ULL
		end
	else
		error(("too many slashes: %s"):format(expr), 2)
	end

	if start < bounds[1] then
		error(("start of the range %s is below minimum %s"):format(start, bounds[1], expr), 2)
	end
	if stop > bounds[2] then
		error(("end of the range %s is above maximum %s"):format(stop, bounds[2], expr), 2)
	end
	if step == 0 then
		error(("step of range %s should be positive number"):format(expr), 2)
	end
	if start > stop then
		error(("end of range cannot be less than start of range %s"):format(expr), 2)
	end
	return bit.bor(get_bits(start, stop, step), extra)
end

local function get_field(field, bounds)
	local bits = 0ULL
	local ranges = field:split(',')
	for _, expr in ipairs(ranges) do
		local b = get_range(expr, bounds)
		bits = bit.bor(bits, b)
	end
	return bits
end

---@param time integer
---@return osdate
local function todate(time)
	return os.date('*t', tonumber(time)) --[[@as osdate]]
end

---@class cron.schedule
---@field minute uint64_t
---@field hour uint64_t
---@field dom uint64_t
---@field month uint64_t
---@field dow uint64_t
local Schedule = {}
Schedule.__index = Schedule
setmetatable(Schedule, { __call = function(_, ...) return Schedule:new(...) end })

function Schedule:new(spec)
	return setmetatable(spec, self)
end

---comment
---@param sched cron.schedule
---@param date osdate
---@return boolean
local function dayMatches(sched, date)
	-- wday: 1 - Sunday, 7 - Saturday
	local domMatch = bit.band(2ULL^(date.day), sched.dom) > 0
	local dowMatch = bit.band(2ULL^(date.wday - 1), sched.dow) > 0

	-- dom or dow == *
	if bit.band(sched.dom, starBit) > 0 or bit.band(sched.dow, starBit) > 0 then
		return domMatch and dowMatch
	end
	return domMatch or dowMatch
end

Schedule.dayMatches = dayMatches

---comment
---@param time integer?
---@return integer|false, string? error_message
function Schedule:next(time)
	time = (time or os.time())+1

	-- for months
	-- for days
	-- for hours
	-- for minutes
	local yearLimit = time + 5*365*86400
	local date = todate(time)

::rewrap::
	local truncated = false
	time = os.time(date)

	if time > yearLimit then
		return false, "year limit overflow"
	end

	-- finding nearest month
	while bit.band(2ULL^date.month, self.month) == 0 do
		if not truncated then
			truncated = true
			date = { year = date.year, month = date.month, day = 1, hour = 0, min = 0, sec = 0 }
		end

		date.month = date.month + 1
		date = todate(os.time(date))
		if date.month == 1 then
			-- year overlap
			goto rewrap
		end
	end

	-- find nearest day
	while not dayMatches(self, date) do
		if not truncated then
			truncated = true
			date = { year = date.year, month = date.month, day = date.day, hour = 0, min = 0, sec = 0 }
		end

		date.day = date.day + 1
		date = todate(os.time(date))

		if date.day == 1 then
			-- month overlap
			-- print("month roll", os.date("%F %T", os.time(date)))
			goto rewrap
		end
	end

	-- find hour
	while bit.band(2ULL^date.hour, self.hour) == 0 do
		if not truncated then
			truncated = true
			date = { year = date.year, month = date.month, day = date.day, hour = date.hour, min = 0, sec = 0 }
		end

		date.hour = date.hour + 1
		date = todate(os.time(date))

		if date.hour == 0 then
			-- day overlap
			-- print("day roll", os.date("%F %T", os.time(date)))
			goto rewrap
		end
	end

	-- find minute
	while bit.band(2ULL^date.min, self.minute) == 0 do
		if not truncated then
			truncated = true
			date = { year = date.year, month = date.month, day = date.day, hour = date.hour, min = date.min, sec = 0 }
		end

		date.min = date.min + 1
		date = todate(os.time(date))

		if date.min == 0 then
			-- hour overlap
			-- print("minute roll", os.date("%F %T", os.time(date)))
			goto rewrap
		end
	end

	-- find second
	while bit.band(2ULL^date.sec, self.second) == 0 do
		date.sec = date.sec + 1
		date = todate(os.time(date))

		if date.sec == 0 then
			-- minute overlap
			goto rewrap
		end
	end

	return os.time(date)
end

---Parses cron expression
---@param spec string
---@return cron.schedule
function M.parse(spec)
	assert(type(spec) == 'string')

	-- TODO: allow TZ specification

	-- Handle named schedules (descriptors), if configured
	if spec:find('@', 1, true) == 1 then
		return Schedule(parse_descriptor(spec))
	end

	local fields = {}
	for field in spec:gmatch("(%S+)") do
		table.insert(fields, field)
	end

	local second = 2ULL^0 -- by default zero second
	if #fields > 5 then
		local sec_f = table.remove(fields, 1)
		second = get_field(sec_f, seconds)
	end

	return Schedule{
		second = second,
		minute = get_field(fields[1], minutes),
		hour   = get_field(fields[2], hours),
		dom    = get_field(fields[3], dom),
		month  = get_field(fields[4], months),
		dow    = get_field(fields[5], dow),
		spec   = spec,
	}
end

return M
