# Crontab parser

Pure Lua crontab parser without any dependencies

## Usage

```lua
    local cron = require 'cron'

    -- linux-like expressions (no seconds)
    local schedule = cron.parse("15 14 1 * *")
    --                           |  |  | | |
    --                           |  |  | | |---- day of week (0-6, Sunday to Saturday)
    --                           |  |  | |-----month (1-12, January to December)
    --                           |  |  |----day of month (1-31)
    --                           |  |-----hour (0-23)
    --                           |-----minute (0-59)
    -- Also @hourly @daily @midnight @weekly @monthly @yearly @annually descriptors are supported

    -- most cron-like libraries (with seconds)
    local schedule = cron.parse("* 15 14 1 * *")
    --                           | |  |  | | |
    --                           | |  |  | | |---- day of week (0-6, Sunday to Saturday)
    --                           | |  |  | |-----month (1-12, January to December)
    --                           | |  |  |----day of month (1-31)
    --                           | |  |-----hour (0-23)
    --                           | |-----minute (0-59)
    --                           |----seconds (0-59)
    -- Also @hourly @daily @midnight @weekly @monthly @yearly @annually descriptors are supported

    local nextTime = schedule:next() -- returns next valid timestamp after `now`
    local nextAfterNext = schedule:next(nextTime) -- returns valid timestamp after `nextTime`
```

## Installation

You may just copy file `cron.lua` into your source code.

No restrictions, no warranties.
